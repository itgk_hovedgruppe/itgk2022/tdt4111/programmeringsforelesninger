# Sjekke om a er nesten lik b.
def nesten_lik(a, b):
    # Matte! :D Sjekke at 99% av b < a < 101% av b
    stor_nok = (a > b * 0.99)
    liten_nok = (a < b * 1.01)

    if stor_nok and liten_nok:
        return True
    return False
# Siden 'return' avslutter funksjonen, trenger vi ikke en 'else' her.


print(nesten_lik(3, 4))
print(nesten_lik(0.3, 0.1+0.2))


'''
Ekstrakommentarer:

En alternativ variant av testen kunne vært noe liknende av:

stor_nok = a > (b - 0.001)
liten_nok = a < (b + 0.001)
Altså legge til/trekke fra et veldig lite tall, heller enn å gange.


Enda en variant: Finne forskjellen og sjekke at den er liten.
Bruker abs() for å finne absoluttverdien (som alltid er positiv).

forskjell = abs(a - b)
if forskjell < 0.001:
    return True
return False
'''
