# Vi lager en liste med tall, som har mange duplikater.
talliste = [1, 2, 2, 3, 3, 2, 3, 1, 3, 2]

# Vi konverterer den til et sett, og skriver ut. Nå er det ingen duplikater.
tallsett = set(talliste)
print(tallsett)

# Mest nyttige et sett kan gjøre for deg: Fjerne duplikater.
# Konverter en liste til et sett og tilbake til en liste. Duplikater fjernet!
talliste2 = list(set([1, 2, 2, 2, 3]))
print(talliste2)
