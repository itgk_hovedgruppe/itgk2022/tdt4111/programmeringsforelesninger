# Iterere gjennom en string, bokstav for bokstav.
for bokstav in 'Hello, World!':
    print(bokstav, end=' ')
print()


# Iterere gjennom tallene i en range.
for tall in range(0, 100, 10):
    print(tall, end=' ')
print()


# Iterere gjennom navnelisten, et navn av gangen.
for navn in ['Paul', 'Karen', 'Bush']:
    print(navn, end=' ')
print()


'''
Det finnes mange itererbare datatyper. De mest relevante er:
* str
* list
* tuple
* range
* dict
* set
'''
