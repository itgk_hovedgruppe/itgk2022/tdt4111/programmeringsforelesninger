# Vi kan hente ut en rekke med verdier/indekser ved å bruke slicing.
heltall = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

# Dårlig metode (med enkelt-indekser).
noen_heltall = [heltall[3], heltall[4], heltall[5]]

# Bra metode (med slicing).
noen_heltall1 = heltall[3:6]
print(noen_heltall1)

# Vi kan hente ut alle verdier fra start til (med ikke med) 5.
noen_heltall2 = heltall[:6]
print(noen_heltall2)

# Vi kan også hente ut alle verdier fra 6 til slutten av listen.
noen_heltall3 = heltall[6:]
print(noen_heltall3)

# Vi kan bruke negative indekser her også.
# Denne koden henter ut de 3 siste verdiene fra listen.
noen_heltall4 = heltall[-3:]
print(noen_heltall4)
