# Vi kan lage en dict med info om f.eks. en president.
president = {'navn': 'Obama', 'land': 'USA', 'alder': 61}
print(president)

# Vi kan også skrive dicten over flere linjer. Det er ofte mer oversiktlig.
bruker = {
    'navn': 'Obama',
    'land': 'USA',
    'alder': 61
}
print(bruker)

# Nå kan vi hente ut verdier basert på vår egen indeks (key).
print(bruker['alder'])

# Og vi kan oppdatere på samme måte.
bruker['alder'] = 59
print(bruker)

# Vi kan også slette verdier i en dict med del.
del bruker['alder']
print(bruker)
