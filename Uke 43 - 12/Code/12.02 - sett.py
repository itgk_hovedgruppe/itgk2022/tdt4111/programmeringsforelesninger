# Vi har en liste med flere duplikate verdier.
# Vi kan lage den til et sett, og da forsvinner duplikater.
liste = [1, 1, 1, 2, 2, 3, 3, 3, 4, 2]
sett = set(liste)
print(sett)

# Vi kan lage den tilbake til en liste. Da er rekkefølgen
# anderledes, men det er en liste med bare enkeltverdier.
liste2 = list(sett)
print(liste2)

# Vi kan gjøre hele denne prosessen på en linje, som vist under.
liste3 = list(set(liste))
print(liste3)
