# Vi kan lage et sett med krøllparenteser {}.
sett1 = {'abc', 123, True, 'mer tekst', 'flere ferdier', 17, 17, 17}
print(sett1)

# Vi kan legge til verdier med add-funksjonen.
sett1.add('14 000')
print(sett1)
 
# Vi kan fjerne en verdi fra et sett med remove.
sett1.remove(17)
print(sett1)

# Vi kan lage et nytt sett, og kombinere det inn i sett1 med update-funksjonen.
sett2 = {1, 2, 3, 4}
sett1.update(sett2)
print(sett1)

# Vi kan finne kombinasjonen av settene med union.
print({'a', 'b', 'c', 1, 2, 3}.union(sett1))

# Vi kan finne verdiene som er i både settA og settB.
print({'a', 'b', 'c', 1, 2, 3}.intersection(sett1))

# Vi kan finne verdiene som bare er i settA, men ikke settB.
print({'a', 'b', 'c', 1, 2, 3}.difference(sett1))

# Vi kan finne verdiene som bare er i settA eller settB, men ikke begge.
print({'a', 'b', 'c', 1, 2, 3}.symmetric_difference(sett1))
