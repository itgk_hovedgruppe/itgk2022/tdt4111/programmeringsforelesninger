a = 3


def f(x):
    global a

    a = 2

    y = x**2
    return y


y = f(3)

# print(y)

print(a)
