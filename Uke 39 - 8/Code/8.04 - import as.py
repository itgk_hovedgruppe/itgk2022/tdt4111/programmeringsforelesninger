# Gi modul et kallenavn ('plt'), så vi slipper å skrive hele greia hver gang.
import matplotlib.pyplot as plt

# (Ikke gått grundig gjennom, fordi ikke så relevant til pensum.)
# Lager to lister med x- og y-verdier.
x_verdier = [0, 1, 2]
y_verdier = [0, 1, 4]

# Plotter verdiene med matplotlib.
plt.plot(x_verdier, y_verdier)
plt.show()


# Kaller 'random' for 'rd', så vi kan skrive mindre.
import random as rd

print(rd.randint(1, 6))
