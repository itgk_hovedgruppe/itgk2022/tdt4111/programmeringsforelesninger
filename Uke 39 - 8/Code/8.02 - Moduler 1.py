# Innebygget modul: Trenger ikke å installere med package manager.
import random

print(random.randint(1, 6))


# Ekstern modul: Må bruke Thonny sin package manager (eller pip).
import numpy

print(numpy.pi)
