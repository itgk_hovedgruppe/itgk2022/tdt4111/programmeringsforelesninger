# Lager navneliste
navneliste = ['Paul', 'Ada', 'Obama', 'Alan']
print(navneliste)

# Oppdaterer andre element (indeks 1) i navnelisten.
# Ada -> Donald Duck
navneliste[1] = 'Donald Duck'

print(navneliste)
