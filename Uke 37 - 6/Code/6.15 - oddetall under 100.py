# Metode 1 - Bruke range(fra, til, steg)
for i in range(1, 100, 2):
    print(i, end=' ')
print()


# Metode 2 - Bruke range(til) og modulærdivisjon (restdivisjon)
for x in range(101):
    if x%2 != 0:
        print(x, end=' ')
