# Her lager vi navnelisten
navneliste = ['Paul', 'Ada', 'Obama', 'Alan']
print(navneliste)

# Her henter vi ut verdien på indeks 1 (andre verdien i listen),
# vi mellomlagrer i variablen "navn", og skriver så ut verdien.
# (Husk at indeks starter på 0, så første element er på indeks 0.)
navn = navneliste[1]

print(navn)

# Vi kunne også bare skrevet verdien direkte ut:
# print(navneliste[1])
