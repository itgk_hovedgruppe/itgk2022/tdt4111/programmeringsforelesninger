## Utenfor pensum for denne uken
# For å kjøre: Installer matplotlib-pakken ved å bruke pakke-manageren.
# Vi skal gå gjennom dette grundigere senere,
# så det er ikke farlig om du ikke får det til. :)

# Ved å legge til importen først pluss de to siste linjene,
# kan vi lage en fin graf av dataene vi har funnet.

# Og ved å bytte ut x-verdiene med en range, kan vi la Python genere
# flere x-verdier for oss, uten at vi trenger å skrive de inn selv.

# Importering
import matplotlib.pyplot as plt


def f(x):
    # return x**2
    y = x**2
    return y


# Lage x-verdier med "range" i stedet for å skrive de inn manuelt.
x_verdier = range(100)
# x_verdier = [0, 1, 2, 3, 4, 5, 6]
y_verdier = []

# Finne y-verdier fra x-verdiene.
for x in x_verdier:
    y = f(x)
    y_verdier.append(y)

print(y_verdier)


# Plotting
plt.plot(x_verdier, y_verdier)
plt.show()
