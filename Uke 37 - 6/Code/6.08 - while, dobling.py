## Oppgave: Hvor mange ganger må vi doble x (7 i dette tilfellet) før det blir
# større enn 10_000?

# Vi setter antall=0, fordi så langt i koden har vi doblet x 0 ganger.
# Vi øker den etterhvert, i loopen.
x = 7
antall = 0

# Mens x er mindre enn 10_000, vil vi gjenta koden inni while-loopen.
# (Doble x og inkrementere antall med 1.)
while x < 10_000:
    x *= 2
    antall += 1

print(antall)
