# Lager navneliste
navneliste = ['Paul', 'Ada', 'Obama', 'Alan']
print(navneliste)

# Legger til 'Donald Duck' på slutten av listen
navneliste.append('Donald Duck')

print(navneliste)

# "Kan jeg legge til et element midt i listen, f.eks. på indeks 2? Ja. Bruk:"
# navneliste.insert(2, 'Donald Duck')
