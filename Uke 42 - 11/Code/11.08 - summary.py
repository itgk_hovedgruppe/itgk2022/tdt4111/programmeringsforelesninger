# Her har vi skrevet programmet på nytt, men det meste
# er puttet inn i funksjoner. Noen av funksjonene har
# fått nytt navn eller er litt endret på.

# To store fordeler med å ha alt i funksjoner:
#   1. Det er lett å teste enkeltfunksjoner, og se at
#         de gjør det de skal gjøre.
#   2. De gjør at det er enklere for oss å kopiere
#         kode til nye programmer.


def les_fra_fil(filnavn):
    '''Leser inn filen _filnavn_.'''
    with open(filnavn, 'r') as f:
        tekst = f.read()
    return tekst


def skriv_til_fil(filnavn, tekst):
    '''Skriver _tekst_ til filen _filnavn_.'''
    with open(filnavn, 'w') as f:
        f.write(tekst)


def splitt_tekst(tekst):
    '''Splitter teksten fra filen til _kolonnetitler_
    og _datatekst_.'''
    linjer = tekst.split('\n')
    kolonnetitler = linjer[0]
    datalinjer = linjer[1:]
    datatekst = '\n'.join(datalinjer)
    return kolonnetitler, datatekst


def konstruer_tabell(tekst):
    '''Lager tekst til tabell. Nesten kopiert fra
    tidligere.'''
    rader = tekst.split('\n')
    tabell = []
    for rad in rader:
        ny_rad = rad.split(', ')
        tabell.append(ny_rad)

    return tabell


def konstruer_string(tabell, kolonnetitler):
    '''Konstruerer csv-teksten fra tabellen vår.
    Mye kopiert.'''
    ny_tekst = kolonnetitler + '\n'
    for rad in tabell:
        linje = ', '.join(rad)
        ny_tekst += linje + '\n'
    return ny_tekst


def print_table(table):
    '''Printe ut tabellen på en pen måte. Kopiert.'''
    for row in table:
        for element in row:
            print(f'{element:25}', end=' ')
        print()


# Lese inn filteksten.
filtekst = les_fra_fil('data/data.csv')

# Separere kolonnetitler og den faktiske dataen.
kolonnetitler, datatekst = splitt_tekst(filtekst)

# Lage tabellen fra datateksten.
tabell = konstruer_tabell(datatekst)

# Skrive ut tabellen.
print_table(tabell)

# Oppdatere tabellen.
tabell[2][2] = 'Bergen-er-best!'

# Konstruere csv-teksten.
ny_csv_tekst = konstruer_string(tabell, kolonnetitler)

# Skrive csv-tekst til ny fil.
skriv_til_fil('data/ny_data.csv', ny_csv_tekst)
