# Denne måten å behandle filer funker også.
# Bruk av 'with'-nøkkelordet er sett på  som bedre,
# fordi hvis linje to av koden krasjer, så får vi ikke
# lukket filen, som kan føre til noen problemer.
# Men jeg har ikke opplevd det selv, så langt jeg kan
# huske, så det er sikkert ikke så farlig. :)
fil = open('data/testfil.txt', 'r')
text = fil.read()
fil.close()

print(text)
