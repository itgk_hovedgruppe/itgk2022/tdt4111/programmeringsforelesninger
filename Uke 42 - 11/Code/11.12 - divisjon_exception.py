# Flere måter å gjøre divisjon på. :D
# 1. Vanlig, uten noen sikkerhet.
# 2. Sjekke om det er lov, før vi gjør det.
# 3. Gjøre det først, og se om det funket.
# It's easier to ask for forgiveness than for promission.

def divisjon1(a, b):
    '''Divisjon: krasjer hvis b == 0.'''
    resultat = a / b
    return resultat


def divisjon2(a, b):
    '''Divisjon: Gjør noe annet hvis b == 0.'''
    if b != 0:
        resultat = a / b
    else:
        resultat = 1_000_000_000
    return resultat


def divisjon3(a, b):
    '''Divisjon: Prøver å dele a på b, og dealer med
    feilen hvis programmet krasjer.'''
    try:
        resultat = a / b
        # [1, 2, 3][17]
    except ZeroDivisionError:
        resultat = 1_000_000_000
    # except IndexError:
    #     print('Indeks-feil!')
    return resultat


print(divisjon3(10, 4))
print(divisjon3(10, 0))
