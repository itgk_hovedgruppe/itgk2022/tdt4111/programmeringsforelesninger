# Bytte verdier i variabler (eller lister)
x = 3
y = 4

# Kan ikke gjøre dette:
# x = y
# y = x
# print(x, y)

# Mellomlagre en av verdiene i en midlertidig variabel.
tmp = x
x = y
y = tmp
print(x, y)  # -> 4, 3

# Tuple-magic.
x, y = y, x
print(x, y)  # -> 4, 3

# I en liste kunne vi f.eks. skrevet følgende
# liste[i], liste[i + 1] = liste[i + 1], liste[i]
