# Noen variabelnavn er litt forbudt. Ikke helt, men litt.
# min, max, print, input, type, round, etc. er innebyggede funksjoner.
# Kaller du en variabel en av de, blir funksjonen "borte" i programmet ditt.
# I.e. du kan ikke bruke den igjen, uten å endre variabelnavn.

min = 0
max = 5
print(min, max)


## Sette "print" som en variabel, og krasje programmet fordi "print"-funksjonen er overskrevet. :c
# print = 4
# print(print)
