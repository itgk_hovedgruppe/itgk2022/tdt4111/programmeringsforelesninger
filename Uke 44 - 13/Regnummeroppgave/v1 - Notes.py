def check_registration(regnummer):
    if not (3 <= len(regnummer) <= 5):
        return False

    if len(regnummer) == 3:
        return (
            regnummer[0].isalpha() and
            regnummer[1].isnumeric() and
            regnummer[2].isalpha()
        )
    elif len(regnummer) == 5:
        return (
            regnummer[0].isalpha() and
            regnummer[1:3].isnumeric() and
            regnummer[3:].isalpha()
        )
    else:
        return (
            regnummer[0].isalpha() and
            regnummer[1].isnumeric() and
            regnummer[2].isalnum() and
            regnummer[3].isalpha()
        )
