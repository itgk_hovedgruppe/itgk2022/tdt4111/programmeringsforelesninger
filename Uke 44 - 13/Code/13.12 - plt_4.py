import matplotlib.pyplot as plt


selskap = ['Equinor', 'Norsk Hydro', 'Yara International', 'Telenor', 'KLP']
inntekt = [566342, 149766, 113837, 113666, 106634]


# Ofte vil vi heller bruke tekst-X-verdier.
# Dette eksempelet er hentet fra forrige ukes forelesning og øvingsoppgaver.
plt.bar(selskap, inntekt)
plt.show()


# For å hente selskap og inntekt fra inntekt_dict fra forrige ukes oppgave:
# selskap = inntekt_dict.keys()
# inntekt = inntekt_dict.values()
