# Med numpy:

# Importere modulen
import numpy as np


# Lage funksjonen
# def f(x):
#     return x**2

# Lage X-verdier, og finne Y-verdier uten å bygge opp en liste fra bunnen.
X = np.arange(0, 6, 1)
# Y = f(X)
Y = X**2  # Versjon som ikke bruker funksjonen

# Printe
print(X)
print(Y)
