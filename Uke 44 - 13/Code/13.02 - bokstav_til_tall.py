# Alle tre funksjonene gjør det samme:
# De mottar en bokstavkarakter, og returnerer en tallkarakter.

# Denne gjør det med en lang if-elif-else-struktur.
def bokstav_til_tall_1(karakter):
    if karakter == 'A':
        return 5
    elif karakter == 'B':
        return 4
    elif karakter == 'C':
        return 3
    elif karakter == 'D':
        return 2
    elif karakter == 'E':
        return 1
    else:
        return 0


# Denne bruker en dictionary som en oppslagstabell.
def bokstav_til_tall_2(karakter):
    lookup = {
        'A': 5,
        'B': 4,
        'C': 3,
        'D': 2,
        'E': 1,
        'F': 0
    }
    return lookup[karakter]


# Denne gjør det samme som forrige, men mer kompakt.
def bokstav_til_tall_3(karakter):
    return {'A': 5, 'B': 4, 'C': 3, 'D': 2, 'E': 1, 'F': 0}[karakter]
