# Samme som forrige, men burker en funksjon.

import numpy as np
import matplotlib.pyplot as plt


def f(x):
    return x**2


X = np.linspace(0, 10, 100)
Y = f(X)

plt.plot(X, Y, '--')

# Vi kan bruke denne for å lagre bildet automatisk.
plt.savefig('f(x).png')
plt.show()
