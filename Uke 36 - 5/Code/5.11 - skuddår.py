## Om denne files:
# I tirsdagsforelesningen hadde vi bare filene 5.09 og 5.12.
# Denne filen legges til sånn at steget fra 5.09 til 5.12 skal vli litt mindre.
# Her lager vi en funksjon som sjekker om det er skuddår i årstallet vi legger inn.
# Ellers gjør koden det samme.
# I filen 5.12 bruker vi denne i en annen funksjon, som gjør ca det samme som denne koden.


# Denne funksjonen returnerer True hvis der er skuddår, og False hvis ikke.
# Dette er basert på årstallet den mottat. F.eks. er 2020 skuddår, men ikke 2022.
def er_skuddår(år):
    # Hvis årstallet er delelig på 4, er det skuddår*.
    #   * Reglene er mer kompliserte, men vi forenkler litt.
    if år % 4 == 0:
        return True
    else:
        return False


årstall = 2022
skuddår_i_år = er_skuddår(årstall)

# Hvis det er skuddår, så er det 29 dager i februar.
if skuddår_i_år:
    antall_dager_i_februar = 29

# Ellers er det 28.
else:
    antall_dager_i_februar = 28

# Skriver ut som en fin beskjed.
print(f'Det er {antall_dager_i_februar} dager i februar i år.')
