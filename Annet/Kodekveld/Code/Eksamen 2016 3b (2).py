def minutt_sekund(sek):
    minutt = int(sek / 60)
    # minutt = sek // 60
    sekund = sek - minutt * 60
    # sekund = sek % 60

    str_sek = str(sekund)
    if len(str_sek) == 1:
        str_sek = '0' + str_sek

    return f'{minutt}:{str_sek}'

print(minutt_sekund(124))
