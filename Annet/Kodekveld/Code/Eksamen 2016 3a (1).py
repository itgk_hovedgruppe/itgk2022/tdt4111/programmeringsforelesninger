def sek_på_banen(lag, bane, tid):
    sek = tid * 60
    sidelinje = lag - bane
    sek_per_spiller = round(sek / lag) * sidelinje
    return sek_per_spiller


print(sek_på_banen(5, 5, 12))
