def myst5(hmm):
    svar = 0
    for x in range(1, len(hmm) - 1):
        if hmm[x - 1] > hmm[x]:
            svar = svar + int(hmm[x])
    return svar


print(myst5('19836565890'))

'''
x	h_x		h_x-1	bool	svar
1	9	<	1		False	0
2	8	<	9		True	8
3	3	<	8		True	3
4	6	<	3		False	0
...


svar = 0 + 8 + 3 + 0 + ...
'''

# rekusjon
def f(x):
    if x < 1:
        return f(x - 1)
    else:
        return 1
