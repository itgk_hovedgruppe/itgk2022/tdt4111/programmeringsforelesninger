def myst(t1,t2):
    n = 2
    while (t1 % n == 0) or (t2 % n == 0):
        n+= 1
    return n

print(myst(49, 42))

'''
(t1 % n == 0) or (t2 % n == 0)
t1 % n == 0
(t1 er delelig med n) eller (t2 er delelig med n)

n	49	42
2	F	T
3	F	T
4	F	F

4
'''
